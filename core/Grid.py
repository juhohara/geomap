
import numpy as np
from osgeo import ogr
from PyQt5 import QtGui, QtCore
from core.Box import Box
from core.WorldTransformer import WorldTransformer
from core.Converter import Converter

class Grid(object):

    def __init__(self, map):

        self.__map = map

        self.__initPointX = None
        self.__initPointY = None

        self.__lineWidth = 1.
        self.__labelOffSetX = 0.
        self.__labelOffSetY = 0.
        self.__angle = 6.

        self.__linesLats = []
        self.__linesLons = []

        self.__pxMapBox = None
        self.__utmBoundingBox = None
        self.__transformer = None

        self.__parallelsGap = 4.
        self.__meridiansGap = 6.

        self._calculateInitials()

        self._graticule() # prepare a graticule (grid)

    def _graticule(self):

        # Generate graticules, grids formed by lines with constant longitude or latitude

        layer = self.__map.getLayer()
        if layer is None:
            return

        self._calculateParallels()
        self._calculateMeridians()

        # transformer projection coordinate to screen coordinates

        s = self.__map.size()
        self.__pxMapBox = Box(0, 0, s.width(), s.height())

        currentBoundingBox = self.__map.getCurrentBoundingBox()

        Converter.change_utm_zone(currentBoundingBox) # if necessary, change the srid of utm by zone ( center point of the box)

        self.__utmBoundingBox = Converter.wgs84box_to(currentBoundingBox)

        # Transformer: utm projection coordinate system to screen coordinate system
        self.__transformer = WorldTransformer()
        self.__transformer.setTransformationParameters(self.__utmBoundingBox, self.__pxMapBox)
        self.__transformer.setMirroring(True)

        # calculate parallels lines
        lons, lats = self._calculateLatitudeLines()
        lons = self.checkParallelsExtremePoints(self.__parallels, lons)
        self.__linesLats = self.latitudeLines(self.__transformer, lons, lats)

        # calculate meridians lines
        lons, lats = self._calculateLongitudeLines()
        lats = self.checkMeridiansExtremePoints(self.__meridians, lats)
        self.__linesLons = self.longitudeLines(self.__transformer, lons, lats)

    def drawParallels(self, painter):

        painter.save()

        for i in range(0, len(self.__linesLats)):

            ring = self.__linesLats[i]

            pt = ring.GetPoint(0)

            px1, py1 = pt[0], pt[1]

            px, py = Converter.lonlat_to_pixel(self.__transformer, px1, py1)

            qPoint = QtCore.QPoint(px, py)

            linePath = QtGui.QPainterPath()
            linePath.moveTo(qPoint)

            for j in range(1, ring.GetPointCount()):

                pt = ring.GetPoint(j)

                px1, py1 = pt[0], pt[1]

                px, py = Converter.lonlat_to_pixel(self.__transformer, px1, py1)

                qPoint = QtCore.QPoint(px, py)
                linePath.lineTo(qPoint)

            painter.drawPath(linePath)

        painter.restore()

    def drawMeridians(self, painter):

        painter.save()

        for i in range(0, len(self.__linesLons)):

            ring = self.__linesLons[i]

            pt = ring.GetPoint(0)
            px1, py1 = pt[0], pt[1]

            px, py = Converter.lonlat_to_pixel(self.__transformer, px1, py1)

            qPoint = QtCore.QPoint(px, py)

            linePath = QtGui.QPainterPath()
            linePath.moveTo(qPoint)

            for j in range(1, ring.GetPointCount()):

                pt = ring.GetPoint(j)
                px1, py1 = pt[0], pt[1]

                px, py = Converter.lonlat_to_pixel(self.__transformer, px1, py1)

                qPoint = QtCore.QPoint(px, py)
                linePath.lineTo(qPoint)

            painter.drawPath(linePath)

        painter.restore()

    def latitudeLines(self, transf, lons, lats):

        lines = []

        for i in range(0, len(lats)):

            geom = ogr.Geometry(ogr.wkbLineString)
            lat = lats[i]

            for j in range(0, len(lons)):

                lon = lons[j]

                geom.AddPoint(lon, lat)

            lines.append(geom)

        return lines

    def longitudeLines(self, transf, lons, lats):

        lines = []

        for i in range(0, len(lons)):

            geom = ogr.Geometry(ogr.wkbLineString)
            lon = lons[i]

            for j in range(0, len(lats)):

                lat = lats[j]

                geom.AddPoint(lon, lat)

            lines.append(geom)

        return lines

    def checkParallelsExtremePoints(self, parallels, lons):

        # check if the last point will be far away the boarder after transformation lat,long to utm

        pxMax = self.__pxMapBox.getWidth()

        for i in range(0, len(parallels)):

            lon = lons[-1]  # last coordinate
            lat = parallels[i] #last coordinate

            px, py = Converter.lonlat_to_pixel(self.__transformer, lon, lat)

            while(px < pxMax):

                lon += 1 # add 1 degree
                lons = np.append(lons, lon) # add new longitude to numpy array
                px, py = Converter.lonlat_to_pixel(self.__transformer, lon, lat)

        return lons

    def checkMeridiansExtremePoints(self, meridians, lats):

        # check if the last point will be far away the boarder after transformation lat,long to utm

        pyMax = self.__pxMapBox.getHeight()

        for i in range(0, len(meridians)):

            lon = meridians[i]  # last coordinate
            lat = lats[-1] #last coordinate

            px, py = Converter.lonlat_to_pixel(self.__transformer, lon, lat)

            while(py < pyMax and py > 0):

                lat += 1 # add 1 degree
                lats = np.append(lats, lat) # add new longitude to numpy array
                px, py = Converter.lonlat_to_pixel(self.__transformer, lon, lat)

        return lats

    def refresh(self):

        # recreate lat and long arrays for new map size or layer
        self._graticule()

    def setParallels(self, parallels):

        self.__parallels = parallels

    def setMeridians(self, meridians):

        self.__meridians = meridians

    def _calculateParallels(self):

        currentBoundingBox = self.__map.getCurrentBoundingBox()
        yMin = currentBoundingBox.get_y1()
        yMax = currentBoundingBox.get_y2()

        if not self.__initPointY is None:
            yMin = self.__initPointY

        self.__parallels = np.arange(yMin, yMax, self.__parallelsGap)

    def _calculateMeridians(self):

        currentBoundingBox = self.__map.getCurrentBoundingBox()
        xMin = currentBoundingBox.get_x1()
        xMax = currentBoundingBox.get_x2()

        if not self.__initPointX is None:
            xMin = self.__initPointX

        self.__meridians = np.arange(xMin, xMax, self.__meridiansGap)

    def _calculateLongitudeLines(self):

        currentBoundingBox = self.__map.getCurrentBoundingBox()
        yMin = currentBoundingBox.get_y1()
        yMax = currentBoundingBox.get_y2()

        if not self.__initPointY is None:
            yMin = self.__initPointY

        lons = list(self.__meridians)
        lats = np.linspace(yMin, yMax, 1000)

        return lons, lats

    def _calculateLatitudeLines(self):

        currentBoundingBox = self.__map.getCurrentBoundingBox()
        xMin = currentBoundingBox.get_x1()
        xMax = currentBoundingBox.get_x2()

        if not self.__initPointX is None:
            xMin = self.__initPointX

        lons = np.linspace(xMin, xMax, 1000)
        lats = list(self.__parallels)

        return lons, lats

    def _calculateInitials(self):

        currentBoundingBox = self.__map.getCurrentBoundingBox()
        self.__initPointX = currentBoundingBox.get_x1()
        self.__initPointY = currentBoundingBox.get_y1()

