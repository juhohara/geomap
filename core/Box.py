
class Box(object):

    def __init__(self, x1, y1, x2, y2):
        self.__x1 = x1
        self.__y1 = y1
        self.__x2 = x2
        self.__y2 = y2

    def get_x1(self):
        return self.__x1
  
    def set_x1(self, x1):
        self.__x1 = x1

    def get_y1(self):
        return self.__y1
  
    def set_y1(self, y1):
        self.__y1 = y1

    def get_x2(self):
        return self.__x2
  
    def set_x2(self, x2):
        self.__x2 = x2

    def get_y2(self):
        return self.__y2
  
    def set_y2(self, y2):
        self.__y2 = y2

    def getWidth(self):
        xmax = 0.
        xmin = 0.

        if(self.__x1 > self.__x2):
            xmax = self.__x1
            xmin = self.__x2
        else:
            xmax = self.__x2
            xmin = self.__x1

            return xmax - xmin

    def getHeight(self):
        ymax = 0.
        ymin = 0.

        if(self.__y1 > self.__y2):
            ymax = self.__y1
            ymin = self.__y2
        else:
            ymax = self.__y2
            ymin = self.__y1

        return ymax - ymin

    def getCenter(self):

        x = self.__x1 + (self.__x2 - self.__x1)/2.
        y = self.__y1 + (self.__y2 - self.__y1)/2.

        return x,y

    def isValid(self):

        if(self.__x1 == 0
        and self.__y1 == 0
        and self.__x2 == 0
        and self.__y2 == 0):
            return False

        if(self.__x1 > self.__x2
           or self.__y1 > self.__y2):
            return False

        return True

    # ao ser usado, chama o get ou set
    x1 = property(fget=get_x1, fset=set_x1)
    # ao ser usado, chama o get ou set
    y1 = property(fget=get_y1, fset=set_y1)
    # ao ser usado, chama o get ou set
    x2 = property(fget=get_x2, fset=set_x2)
    # ao ser usado, chama o get ou set
    y2 = property(fget=get_y2, fset=set_y2)
