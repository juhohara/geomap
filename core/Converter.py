
from osgeo import gdal
from osgeo import osr
from core.Box import Box
from pyproj import Proj, transform
import math

"""
    OGR Geometry Types

    - 0 Geometry
    - 1 Point
    - 2 Line
    - 3 Polygon
    - 4 MultiPoint
    - 5 MultiLineString
    - 6 MultiPolygon
    - 100 No Geometry
"""

class Converter(object):

    sourceProj = Proj('+init=epsg:4326')  # static variable
    targetProj = Proj('+init=epsg:32722') # static variable

    @staticmethod
    def convertLinearRingTo(transform, linearRing):

        if not linearRing.GetGeometryType() is 2:  # LinearRing
            return linearRing

        x = 0
        y = 0

        for j in range(0, linearRing.GetPointCount()):
            # GetPoint return a tuple not a Geometry
            pt = linearRing.GetPoint(j)

            p1, p2 = Converter.transform_wgs84_to(pt[0], pt[1])

            x, y = transform.system1Tosystem2(p1, p2)
            linearRing.SetPoint(j, x, y)

        return linearRing

    @staticmethod
    def convertLatLongArrayTo(transform, latArray, longArray):

        x = 0
        y = 0

        for i in range(0, len(latArray)):

            lat = latArray[i]
            long = longArray[i]

            p1, p2 = Converter.transform_wgs84_to(long, lat)
            x,y = transform.system1Tosystem2(p1, p2)
            latArray[i] = x
            longArray[i] = y

        return latArray, longArray

    @staticmethod
    def convertLatArrayTo(transform, array):

        y = 0

        for i in range(0, len(array)):
            lat = array[i]
            p1, p2 = Converter.transform_wgs84_to(0, lat)
            y = transform.ySystem1ToSystem2(p2)
            array[i] = y

        return array

    @staticmethod
    def convertLongArrayTo(transform, array):

        x = 0

        for i in range(0, len(array)):
            lon = array[i]
            p1, p2 = Converter.transform_wgs84_to(lon, 0)
            x = transform.xSystem1ToSystem2(p1)
            array[i] = x

        return array

    @staticmethod
    def transform_utm_to_wgs84(easting, northing, zone):

        utm_coordinate_system = osr.SpatialReference()
        utm_coordinate_system.SetWellKnownGeogCS("WGS84")  # Set geographic coordinate system to handle lat/lon
        is_northern = northing > 0
        utm_coordinate_system.SetUTM(zone, is_northern)

        wgs84_coordinate_system = utm_coordinate_system.CloneGeogCS()  # Clone ONLY the geographic coordinate system

        # create transform component
        utm_to_wgs84_geo_transform = osr.CoordinateTransformation(utm_coordinate_system, wgs84_coordinate_system)  # (, )
        return utm_to_wgs84_geo_transform.TransformPoint(easting, northing, 0)  # returns lon, lat, altitude

    @staticmethod
    def get_utm_zone(longitude):
        return (int(1 + (longitude + 180.0) / 6.0))

    @staticmethod
    def is_northern(latitude):
        """
        Determines if given latitude is a northern for UTM
        """
        if (latitude < 0.0):
            return 0
        else:
            return 1

    @staticmethod
    def transform_wgs84_to(lon, lat):

        easting, northing = transform(Converter.sourceProj, Converter.targetProj, lon, lat)
        return easting, northing

    @staticmethod
    def lonlat_to_pixel(utmpixelTransform, lon, lat):
        """
        Translates the given lon, lat to the grid pixel coordinates in data array (zero start)
        """
        # transform to utm
        x, y = Converter.transform_wgs84_to(lon, lat)

        # apply inverse geo tranform
        pixel_x, pixel_y = utmpixelTransform.system1Tosystem2(x, y)

        return pixel_x, pixel_y

    @staticmethod
    def wgs84box_to(wgs84box):

        xMin = wgs84box.get_x1()
        xMax = wgs84box.get_x2()
        yMin = wgs84box.get_y1()
        yMax = wgs84box.get_y2()

        lower_right_x, lower_right_y = transform(Converter.sourceProj, Converter.targetProj, xMin, yMin)
        top_left_x, top_left_y = transform(Converter.sourceProj, Converter.targetProj, xMax, yMax)

        utmBoundingBox = Box(lower_right_x, lower_right_y, top_left_x, top_left_y)

        return utmBoundingBox

    @staticmethod
    def change_utm_zone(boundingBox):

        # change utm zone from bounding box center
        # interest use because lines will be more curved on pan
        lon, lat = boundingBox.getCenter()

        zone = Converter.get_utm_zone(lon)
        is_northern = Converter.is_northern(lat)

        srid = 32722 # center point Brazil

        if(is_northern):
            #northern hemisphere
            srid = 32600 + zone
        else:
            # southern hemisphere
            srid = 32700 + zone

        epsg = '+init=epsg:' + str(srid)

        Converter.targetProj = Proj(epsg)

        return zone
