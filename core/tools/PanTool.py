
from PyQt5 import QtGui, QtWidgets, QtCore
from core.tools.AbstractTool import AbstractTool
from core.Box import Box

class PanTool(AbstractTool):

    def __init__(self, map, cursor, actionCursor = QtGui.QCursor(), parent = None):

        super(PanTool, self).__init__(map, parent)

        self._cursor = cursor
        self.__actionCursor = actionCursor
        self.__started = False
        self.__origin = None
        self.__delta = QtCore.QPoint()
        self.__reference = None

    def mousePressEvent(self, e):

        if (e.button() != QtCore.Qt.LeftButton):
            return False

        self.__started = True
        self.__origin = e.pos()
        self.__delta *= 0
        self.__reference = self._map.transform(e.pos())

        if (self.__actionCursor.shape() != QtCore.Qt.BlankCursor):
            self._map.setCursor(self.__actionCursor)

        return True

    def mouseMoveEvent(self, e):

        if not self.__started:
            return False

        self.__delta = e.pos() - self.__origin

        return True

    def mouseReleaseEvent(self, e):

        self.__started = False

        self._map.setCursor(self._cursor)

        if (e.button() != QtCore.Qt.LeftButton or self.__delta.isNull()):
            return False

        extent = self._map.getCurrentBoundingBox()
        width = extent.getWidth()
        height = extent.getHeight()

        x,y = extent.getCenter()

        oldCenter = QtCore.QPointF(x, y)

        releasePoint = QtCore.QPointF(self._map.transform(e.pos()))

        dx = releasePoint.x() - self.__reference.x()
        dy = releasePoint.y() - self.__reference.y()

        newCenter = QtCore.QPointF(oldCenter.x() - dx, oldCenter.y() + dy)

        newExtent = Box(newCenter.x() - (width / 2.), newCenter.y() - (height / 2.), newCenter.x() + (width / 2.),
                  newCenter.y() + (height / 2.))

        self._map.setCurrentBoundingBox(newExtent)
        self._map.refresh()

        return True

