
from PyQt5 import QtGui, QtWidgets, QtCore

class AbstractTool(QtCore.QObject):

    def __init__(self, map, parent = None):

        super(AbstractTool, self).__init__(parent)

        self._map = map
        self._cursor = QtGui.QCursor(QtCore.Qt.BlankCursor)

    def eventFilter(self, watched, e):

        if (e.type() == QtCore.QEvent.MouseButtonPress):

            return self.mousePressEvent(e)

        if (e.type() == QtCore.QEvent.MouseMove):

            return self.mouseMoveEvent(e)

        if (e.type() == QtCore.QEvent.MouseButtonRelease):

            return self.mouseReleaseEvent(e)

        if (e.type() == QtCore.QEvent.MouseButtonDblClick):

            return self.mouseDoubleClickEvent(e)

        if (e.type() == QtCore.QEvent.Enter):

            if (self._cursor.shape() != QtCore.Qt.BlankCursor):

                self._map.setCursor(self._cursor)
                return False

        return QtCore.QObject.eventFilter(self, watched, e)

    def mousePressEvent(self, e):
        return False

    def mouseMoveEvent(self, e):
        return False

    def mouseReleaseEvent(self, e):
        return False

    # return bool
    def mouseDoubleClickEvent(self, e):
        return False

    def setCursor(self, cursor):
        self._cursor = cursor


