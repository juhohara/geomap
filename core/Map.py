
from PyQt5 import QtGui, QtWidgets, QtCore
from osgeo import ogr
from core.Converter import Converter
from core.WorldTransformer import WorldTransformer
from core.Box import Box
from core.Grid import Grid

"""
    OGR Geometry Types

    - 0 Geometry
    - 1 Point
    - 2 Line
    - 3 Polygon
    - 4 MultiPoint
    - 5 MultiLineString
    - 6 MultiPolygon
    - 100 No Geometry

    *Obs: OGRDataSource inherits from GDALDataset
"""

class Map(QtWidgets.QWidget):

    def __init__(self, path, parent = None):

        super(Map, self).__init__(parent)

        self.__path = path

        self.__penColor = QtGui.QColor(0, 0, 0)
        self.__brushColor = QtGui.QColor(22, 160, 133)
        self.__transparentBrushColor = QtGui.QColor(22, 160, 133, 0)
        self.__backgroundColor = QtGui.QColor(255, 255, 255)
        self.__pen = QtGui.QPen(self.__penColor, 0, QtCore.Qt.DashLine)

        self.__layer = None
        self.__geoms = []
        self.__points = []
        self.__dataSource = None
        self.__layer = None
        self.__transformer = None
        self.__wgs84Transformer = None

        self.initialize()

        self.__grid = Grid(self)

    def initialize(self):

        s = QtCore.QSize(700, 500)
        self.resize(s)
        self.setAutoFillBackground(True)

        pal = self.palette()
        pal.setColor(QtGui.QPalette.Background, self.__backgroundColor)
        self.setPalette(pal)

        self.__layer, self.__dataSource = self.importShapeFile()

        x_min, y_min, x_max, y_max = self.__layer.GetExtent()

        self.__sourceBoundingBox = Box(x_min, y_min, x_max, y_max)
        self.__currentBoundingBox = Box(x_min, y_min, x_max, y_max)

        self.showInfo()

    def showInfo(self):

        print('--- Data source info ---')
        print('Shapefile path: %s' % self.__dataSource.GetName())
        print('Layer count: %d \n' % self.__dataSource.GetLayerCount())

        print('--- Layer info ---')
        print('Layer name: %s' % self.__layer.GetName())
        print('Geom type: %s' % self.__layer.GetGeomType())
        print('Extent: ', self.__layer.GetExtent())
        print('Spatial Reference: ', self.__layer.GetSpatialRef())
        print('Feature count: %d \n' % self.__layer.GetFeatureCount())

    def importShapeFile(self):

        #Import shapefile

        #Get Shapefile driver
        driver = ogr.GetDriverByName('ESRI Shapefile')

        #Open shapefile and get data source
        dataSource = driver.Open(self.__path, 0) # 0 means read-only. 1 means writeable

        layer = None

        #Check if is valid
        if dataSource is None:
            print ('Could not open %s' % self.__path)
        else:
            layer = dataSource.GetLayer()

        return layer, dataSource


    def _drawMap(self, painter):

        if self.__layer is None:
            print('Could not draw the Shapefile, layer is missing.')
            return

        s = self.size()

        pixelBox = Box(0, 0, s.width(), s.height())

        # drawing map and grids on the same projection.
        # In this case, planar projection (utm) will be used.
        projectBoundingBox = Converter.wgs84box_to(self.__currentBoundingBox)
        # Transformer: projection coordinate system to screen coordinate system
        self.__transformer = WorldTransformer()
        self.__transformer.setTransformationParameters(projectBoundingBox, pixelBox)
        self.__transformer.setMirroring(True)

        # Transformer: projection coordinate system to screen coordinate system
        self.__wgs84Transformer = WorldTransformer()
        self.__wgs84Transformer.setTransformationParameters(self.__currentBoundingBox, pixelBox)
        self.__wgs84Transformer.setMirroring(True)

        painter.save()

        for feature in self.__layer:
            geom = feature.GetGeometryRef()
            self._drawPolygon(painter, geom, self.__transformer)

        painter.restore()

        self.__layer.ResetReading() # Must call Reset Reading if we want to start iterating over the layer again

    def _drawPoint(self, painter, geom):

        if self.__layer.GetGeomType() != 1:
            return

    def _drawLine(self, painter, geom):

        if self.__layer.GetGeomType() != 2:
            return

    def _drawPolygon(self, painter, geom, transformer):

        # Draw Polygon

        if geom.GetGeometryType() is 3 or geom.GetGeometryType() is 6: # Polygon

            polygon = QtGui.QPolygonF()

            for i in range(0, geom.GetGeometryCount()):

                ring = geom.GetGeometryRef(i)
                Converter.convertLinearRingTo(transformer, ring)

                self._drawPolygon(painter, ring, transformer)

                for j in range(0, ring.GetPointCount()):
                    # GetPoint return a tuple not a Geometry
                    pt = ring.GetPoint(j)
                    qPoint = QtCore.QPoint(pt[0], pt[1])
                    polygon.append(qPoint)

            painter.drawPolygon(polygon)

    def paintEvent(self, event):

        painter = QtGui.QPainter()

        painter.begin(self)

        painter.setPen(self.__pen)
        painter.setBrush(self.__brushColor)

        self._drawMap(painter) # draw shapefile geometries

        painter.setBrush(self.__transparentBrushColor)

        self.__grid.drawParallels(painter) # draw parallels lines
        self.__grid.drawMeridians(painter)  # draw meridians lines

        painter.end()

    def getLayer(self):

        return self.__layer

    def getCurrentBoundingBox(self):

        return self.__currentBoundingBox

    def setCurrentBoundingBox(self, boundingBox):

        self.__currentBoundingBox = boundingBox

    def resizeEvent(self, event):

        self.refresh()

    def refresh(self):

        self.__grid.refresh()
        self.update()

    def transform(self, pos):

        # transform screen coordinate to map coordinate

        newPoint = pos

        if self.__wgs84Transformer is None:
           return newPoint

        x, y = self.__wgs84Transformer.system2Tosystem1(pos.x(), pos.y())

        newPoint = QtCore.QPointF(x, y)

        return newPoint
