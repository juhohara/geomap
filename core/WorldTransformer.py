
"""
    Class to transform from System 1 coordinate to other type of coordinate system (System 2) and vice-versa.
"""

class WorldTransformer:

    def __init__(self):

        self.__scaleX = 0.
        self.__scaleY = 0.
        self.__translateX = 0
        self.__translateY = 0
        self.__s1llx = 0.
        self.__s1lly = 0.
        self.__s1urx = 0.
        self.__s1ury = 0.
        self.__s1Width = 0.
        self.__s1Height = 0.
        self.__s2llx = 0.
        self.__s2lly = 0.
        self.__s2urx = 0.
        self.__s2ury = 0.
        self.__s2Width = 0.
        self.__s2Height = 0.
        self.__valid = False
        self.__mirroring = True

    def setTransformationParameters(self, system1Box, system2Box):

        self.__valid = True

        if not system1Box.isValid():
            return
        if not system2Box.isValid():
            return

        self._initVariables(system1Box, system2Box)

        self.__scaleX = self.__s2Width / self.__s1Width
        self.__scaleY = self.__s2Height / self.__s1Height

        self.__translateX = self.__s2llx - self.__s1llx * self.__scaleX
        self.__translateY = self.__s2lly - self.__s1lly * self.__scaleY

    def system1Tosystem2(self, wx, wy):

        wx = (self.__scaleX * wx) + self.__translateX
        wy = (self.__scaleY * wy) + self.__translateY

        if (self.__mirroring):

            dyCopy = wy
            wy = self.__s2Height - 1 - dyCopy #mirror

        return wx, wy

    def xSystem1ToSystem2(self, wx):

        wx = (self.__scaleX * wx) + self.__translateX

        return wx

    def ySystem1ToSystem2(self, wy):

        wy = (self.__scaleY * wy) + self.__translateY

        if (self.__mirroring):
            dyCopy = wy
            wy = self.__s2Height - 1 - dyCopy  # mirror

        return wy

    def system2Tosystem1(self, dx, dy):

        dyCopy = dy

        if (self.__mirroring):

            dy = self.__s2Height - 1 - dyCopy; #mirror

        dx = (dx - self.__translateX) / self.__scaleX
        dy = (dyCopy - self.__translateY) / self.__scaleY

        return dx, dy

    def _initVariables(self, system1Box, system2Box):

        self.__scaleX = 0.
        self.__scaleY = 0.

        self.__translateX = 0.
        self.__translateY = 0.

        self.__s1llx = system1Box.get_x1()
        self.__s1lly = system1Box.get_y1()
        self.__s1urx = system1Box.get_x2()
        self.__s1ury = system1Box.get_y2()
        self.__s1Width = system1Box.getWidth()
        self.__s1Height = system1Box.getHeight()

        self.__s2llx = system2Box.get_x1()
        self.__s2lly = system2Box.get_y1()
        self.__s2urx = system2Box.get_x2()
        self.__s2ury = system2Box.get_y2()
        self.__s2Width = system2Box.getWidth()
        self.__s2Height = system2Box.getHeight()

    def getScaleX(self):

        return self.__scaleX

    def getScaleY(self):

        return self.__scaleY

    def getTranslateX(self):

        return self.__translateX

    def getTranslateY(self):

        return self.__translateY

    def getS1llx(self):

        return self.__s1llx

    def getS1lly(self):

        return self.__s1lly

    def getS1urx(self):

        return self.__s1urx

    def getS1ury(self):

        return self.__s1ury

    def getS1Width(self):

        return self.__s1Width

    def getS1Height(self):

        return self.__s1Height

    def getS2llx(self):

        return self.__s2llx

    def getS2lly(self):

        return  self.__s2lly

    def getS2urx(self):

        return self.__s2urx

    def getS2ury(self):

        return self.__s2ury

    def getS2Width(self):

        return self.__s2Width

    def getS2Height(self):

        return self.__s2Height

    def isValid(self):

        if ((self.__scaleX <= 0.) and (self.__scaleY <= 0.)):
            return False

        return True

    def isMirroring(self):

        return self.__mirroring

    def setMirroring(self, mirror):

        self.__mirroring = mirror


