Description: Instructions for setting up the environment for the  GeoMap (Prototype for testing geoprocessing features of GIS systems).
Author: Juliana Hohara de Souza Coelho <https://gitlab.com/juhohara>

To run this project you must have installed:

- Python 3.5
- PyQt 5
- Geos
- GDAL
- NumPy
- PyProj







