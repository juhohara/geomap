from distutils.core import setup

setup(
    name='geomap',
    version='1.0',
    packages=['core', 'default', 'test'],
    url='',
    license='',
    author='Juliana Hohara de Souza Coelho',
    author_email='jujuhohara@gmail.com',
    description='Prototype in Python/PyQt5 for testing geoprocessing features of GIS systems.'
)
