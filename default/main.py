
import sys
from PyQt5 import QtWidgets, QtGui
from core.Map import Map
from default.GeoMap import GeoMap

if __name__ == '__main__':

    # Must construct a QApplication before a QWidget
    app = QtWidgets.QApplication(sys.argv)

    path = '../../data/Estados/estados_pol.shp'

    geoMapApp = GeoMap()
    map = Map(path, geoMapApp)

    geoMapApp.setMapComponent(map)
    geoMapApp.setVisible(True)

    sys.exit(app.exec_())