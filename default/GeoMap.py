
from PyQt5 import QtGui, QtWidgets, QtCore
from core.tools.PanTool import PanTool

"""
    Main Window of GeoMap System
"""

class GeoMap(QtWidgets.QMainWindow):

    def __init__(self, parent = None):

        super(GeoMap, self).__init__(parent)

        self.setWindowTitle("GeoMap - Geographic Information System")

        s = QtCore.QSize(700, 500)
        self.resize(s)

        self.__mapComponent = None
        self.__currentTool = None

        self.initializeToolBar()

    def initializeToolBar(self):

        self.__toolbar = QtWidgets.QToolBar(self)

        btn = self.createToolButton("Pan", "Pan Tool", None)
        self.__toolbar.addWidget(btn)
        self.__toolbar.addSeparator()

        btn.clicked.connect(self.onPanClicked) # signal/slot on clicked action

        self.addToolBar(self.__toolbar)

    def setMapComponent(self, map):

        self.__mapComponent = map

        if not self.__mapComponent is None:
            self.setCentralWidget(self.__mapComponent)

    def onPanClicked(self, checked):

        if self.__mapComponent is None:
            return

        if(checked == True):
            cursor = QtGui.QCursor(QtCore.Qt.ArrowCursor)
            actionCursor = QtGui.QCursor(QtCore.Qt.SizeAllCursor)
            self.__currentTool = PanTool(self.__mapComponent, cursor, actionCursor)
            self.__mapComponent.installEventFilter(self.__currentTool)
        else:
            if not self.__currentTool is None:
                self.__mapComponent.removeEventFilter(self.__currentTool)
                self.__currentTool = None

    def createToolButton(self, text, tooltip, iconName):

        btn = QtWidgets.QToolButton(self.__toolbar)

        btn.setText(text)
        btn.setToolTip(tooltip)

        btn.setGeometry(0,0,20,20)
        btn.setCheckable(True)

        if not iconName is None:

            iconPath = ":/AppRes/" + iconName
            btn.setIcon(iconPath)

        return btn

